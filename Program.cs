﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HomeWork2
{

    class Program
    {
        static void Main(string[] args)
        {
            Person[] Persons = new Person[]
            {
                new Person("Aaa", 22),
                new Person("Bbb", 32),
                new Person("ccc", 33),
                new Person("dddd", 44),
                new Person("Eddd", 11)

            };

            Console.WriteLine("Транслируем список в объект XML:");

            ISerializer serializer = new OtusXmlSerializer();

            string dataXML = serializer.Serialize(Persons);

            File.WriteAllText("./persons.xml", dataXML);
            Console.WriteLine(dataXML);
            Console.WriteLine();
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(dataXML));

            Console.WriteLine("Десериализуем XML:");
            StreamReader<Person> StreamReader = new StreamReader<Person>(stream, serializer);
            foreach (var ppl in StreamReader)
            {
                Console.WriteLine(ppl.ToString());
            }

            PersonSorter sorter = new PersonSorter();

            var sorted = sorter.Sort<Person>(Persons, "Age");
            Console.WriteLine();
            Console.WriteLine("Отсортированный список:");

            foreach (Person s in sorted)
            {
                Console.WriteLine(s.ToString());
            }

            Console.WriteLine();

            var repo = new AccountRepository("TestFile.txt");

            var accServ = new AccountService(repo);

            accServ.AddAccount(new Account()
            {
                FirstName = "Name1",
                LastName = "LastName1",
                BirthDate = new DateTime(1977, 01, 01)
            });
            accServ.AddAccount(new Account()
            {
                FirstName = "Name2",
                LastName = "LastName2",
                BirthDate = new DateTime(1978, 02, 02)
            });
            accServ.AddAccount(new Account()
            {
                FirstName = "Name3",
                LastName = "LastName3",
                BirthDate = new DateTime(1979, 03, 03)
            });
            accServ.AddAccount(new Account()
            {
                FirstName = "Name4",
                LastName = "LastName4",
                BirthDate = new DateTime(1980, 04, 04)
            });

            var txt = repo.GetAll();
            foreach (var element in txt)
            {
                Console.WriteLine("First name - {0}\n Last name - {1}\n Birth date - {2}",
                    element.FirstName,
                    element.LastName,
                    element.BirthDate);
                Console.WriteLine();
            }

            Console.ReadKey();

        }

    }
}
