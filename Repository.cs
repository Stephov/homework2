﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Xml.Serialization;
using ExtendedXmlSerializer.Configuration;
using ExtendedXmlSerializer;


namespace HomeWork2
{
	class AccountRepository : IRepository<Account>
	{
		private string FilePath { get; set; }
		public AccountRepository(string path)
		{
			FilePath = path;
			if (!File.Exists(path))
				File.WriteAllText(path, string.Empty);
		}
		public void Add(Account item)
		{
			var contents = new string[]
			{
				string.Join(";", item.FirstName, item.LastName, item.BirthDate)
			};

			File.AppendAllLines(FilePath, contents);
		}

		public IEnumerable<Account> GetAll()
		{
			var accList = new List<Account>();
			var text = File.ReadAllText(FilePath);
			var rows = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var row in rows)
			{
				var columns = row.Split(new string[] { ";" }, StringSplitOptions.None);
				if (columns.Length < 3) continue;
				accList.Add(new Account()
				{
					FirstName = columns[0],
					LastName = columns[1],
					BirthDate = DateTime.Parse(columns[2])
				});
			}
			return accList;
		}

		public Account GetOne(Func<Account, bool> predicate)
		{
			return GetAll().FirstOrDefault(a => predicate(a));
		}

	}



}
