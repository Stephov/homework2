using HomeWork2;
using Moq;
using NUnit.Framework;

namespace NUnitTest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestAddAccount()
        {
            var mock = new Mock<IRepository<Account>>();
            mock
                .Setup(rp => rp.Add(It.IsAny<Account>())).Verifiable(); 

            IAccountService accountService = new AccountService(mock.Object);
            accountService.AddAccount(new Account()
            {
                FirstName = "",
                LastName = "",
                BirthDate = new System.DateTime(1996, 10, 09)
            });
            accountService.AddAccount(new Account()
            {
                FirstName = "",
                LastName = "",
                BirthDate = new System.DateTime(2010, 10, 09)
            });

            mock.Verify(repository => repository.Add(It.IsAny<Account>()));
        }

    }
}