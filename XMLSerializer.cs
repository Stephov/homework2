﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace HomeWork2
{


    public interface ISerializer
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
    }

    public class OtusXmlSerializer: ISerializer
    {
        private static readonly XmlWriterSettings XmlWriterSettings;
        private static readonly XmlReaderSettings XmlReaderSettings;

        static OtusXmlSerializer()
        {
            XmlWriterSettings = new XmlWriterSettings { Indent = true };
            XmlReaderSettings = new XmlReaderSettings { IgnoreWhitespace = false };
        }

        public string Serialize<T>(T item)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer()
              .UseAutoFormatting()
              .UseOptimizedNamespaces()
              .EnableImplicitTyping(typeof(T))
              .Create();

            return serializer.Serialize(XmlWriterSettings, item);
        }

        public T Deserialize<T>(Stream stream)
        {
            IExtendedXmlSerializer deSerializer = new ConfigurationContainer()
              .UseAutoFormatting()
              .UseOptimizedNamespaces()
              .EnableImplicitTyping(typeof(T))
              .Create();

            return deSerializer.Deserialize<T>(XmlReaderSettings, stream);
        }
    }

}
