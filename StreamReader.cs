﻿using ExtendedXmlSerializer.ContentModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2
{
    public class StreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly Stream Data;
        private readonly ISerializer Serializer;

        public StreamReader(Stream data, ISerializer serializer)
        {
            Data = data;
            Serializer = serializer;
        }
        public IEnumerator<T> GetEnumerator()
        {
            T[] desArr = Serializer.Deserialize<T[]>(Data);

            if (Data != null)
            {
                foreach (var element in desArr)
                {
                    yield return element;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            if (Data != null)
            {
                Data.Close();
            }
            GC.SuppressFinalize(this);
        }
    }
}