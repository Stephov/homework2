﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2
{
    public interface ISorter<T>
    {
        IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems, string property);
    }

    public class PersonSorter : ISorter<Person>
    {
        public IEnumerable<Person> Sort<Person>(IEnumerable<Person> notSortedItems, string property)
        {
            return from u in notSortedItems
                   orderby u.GetType().GetProperty(property).GetValue(u, null)
                   select u;
        }
    }


}