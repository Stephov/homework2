﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork2
{

    public class AccountService : IAccountService
    {
        private readonly IRepository<Account> Repository;
        public AccountService(IRepository<Account> repository)
        {
            Repository = repository;
        }
        public void AddAccount(Account account)
        {
            if (string.IsNullOrEmpty(account.FirstName))
            {
                throw new Exception("Заполните Имя");
            }
            if (string.IsNullOrEmpty(account.LastName))
            {
                throw new Exception("Заполните Фамилию");
            }

            DateTime today = DateTime.Today;
            TimeSpan age = today - account.BirthDate;
            double ageInYears = age.TotalDays / 365;

            if (ageInYears < 18)
            {
                throw new Exception("Возраст должен быть больше 18 лет");
            }
            else
            {
                Repository.Add(account);
            }
        }
    }

}